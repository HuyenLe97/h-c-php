<?php

require 'db_connect.php';

    $error = array('ma'=>'', 'hoten'=>'');

if(isset($_POST['submit']) ){
                
    $NgaySinh= $_POST["NgaySinh"];
    $Lop= $_POST["Lop"];
    $MaHS= $_POST["MaHS"];
    $HoTen = $_POST["HoTen"];
    $sql = mysqli_query($conn,"SELECT * FROM hocsinh where MaHS='$MaHS'");
    $row = mysqli_fetch_array($sql);

    if(empty($MaHS) || trim($MaHS)=="")
        {
            $error['ma']=" <span style='color:red'>*Mã học sinh không được để trống hoặc khoảng trắng!* </span>";
        }
    else
        {
            $MaHS;
        }
    if(empty($HoTen) || trim($HoTen)=="")
        {
            $error['hoten']=" <span style='color:red'>*Họ tên học sinh không được để trống hoặc khoảng trắng!* </span>";
        }
    else
        {
            $HoTen ;
        }
    if($row > 0 )
    {
        $error['ma']=" <span style='color:red'>*Mã học sinh đã tồn tại!* </span>";
    }
    
    if(!$error['hoten']&&!$error['ma']){
        //INSERT USERS DATA INTO THE DATABASE
        $insert_query = mysqli_query($conn,"INSERT INTO hocsinh VALUES('$MaHS','$HoTen','$NgaySinh','$Lop')");
        
        //CHECK DATA INSERTED OR NOT
        if($insert_query)
        {
             header('Location:index.php?status=success');
           
        }
        else{
            echo "<h3> Opps something wrong! </h3>";
        }
    }
} 
   
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
    <div class="col-lg-8 col-lg-offset-2">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Thêm mới học sinh </h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="index.php"> Quay lại </a>
                </div>
            </div>
        </div>
        <form method="post" action="create.php">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="form-group">
                        <form autocomplete="off">
                            <label for="MaHS">Mã học sinh: </label>
                            <input name="MaHS" placeholder="HS0..."  type="text" value="<?php if(isset($MaHS)){ echo $MaHS; } ?>"  class="form-control" />
                            <div><?php echo $error['ma'] ?></div>

                        </form>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Họ tên:</strong>
                        <input type="text" name="HoTen" value="<?php if(isset($HoTen)){ echo $HoTen; }?>" class="form-control">
                        <div><?php echo $error['hoten'] ?></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Ngày sinh:</strong>
                        <input type="text" name="NgaySinh"  value="<?php if(isset($NgaySinh)){ echo $NgaySinh; }?>" class="form-control">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Lớp:</strong>
                        <input type="text" name="Lop"  value="<?php if(isset($Lop)){ echo $Lop ;}?>" class="form-control">

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit"  name="submit" class="btn btn-primary" >Thêm</button>
                    
                </div>
            </div>


        </form>
    </div>

</body>

</html>