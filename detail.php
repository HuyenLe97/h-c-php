<?php
include 'db_connect.php';
    $MaHS = $_GET["id"];
    $sql = mysqli_query($conn,"SELECT * FROM hocsinh where MaHS='$MaHS'");
    $row = mysqli_fetch_array($sql);
      
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Thông tin chi tiết</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
        <div class="col-lg-8 col-lg-offset-2" style="border-style: double ; margin-top:50px; margin-left:450px;width:40%">
            <div class="row" style="width:550px; color:blue; text-align:center;">
                <div class="col-lg-12 margin-tb " style="height:70px">
                    <div >
                        <h2 > Thông tin học sinh </h2>
                    </div>
                    <div class="pull-right" >
                        <a class="btn btn-primary" href="index.php"> Quay lại </a>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 60%; margin-left:70px">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Mã học sinh: </strong>
                        <span><?php echo $row["MaHS"] ?> </span>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12" styles>
                    <div class="form-group">
                        <strong>Họ tên: </strong>
                        <span><?php echo $row["HoTen"] ?> </span>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12" >
                    <div class="form-group">
                        <strong> Ngày sinh: </strong>
                        <span><?php echo $row["NgaySinh"] ?> </span>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Lớp:</strong>
                        <span ><?php echo $row["Lop"] ?> </span>
                    </div>
                </div>
               
            </div>
        </div>
    

</body>

</html>