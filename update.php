<?php
include 'db_connect.php';
    $MaHS = $_GET["id"];
    $sql = mysqli_query($conn,"SELECT * FROM hocsinh where MaHS='$MaHS'");
    $row = mysqli_fetch_array($sql);
      
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Update</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
    <form method="post" action="updated.php">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2 style="color:blue">Sửa thông tin học sinh </h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="index.php"> Quay lại </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>MaHS</strong>
                        <input type="hidden" name="MaHS" value="<?php echo $row["MaHS"] ?>" class="form-control">

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Họ tên:</strong>
                        <input type="text" name="HoTen" value="<?php echo $row["HoTen"]; ?>" class="form-control">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong> Ngày sinh: </strong>
                        <input type="text" name="NgaySinh" value="<?php echo $row["NgaySinh"]; ?>" class="form-control">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Lớp:</strong>
                        <input type="text" name="Lop" value="<?php echo $row["Lop"]; ?>" class="form-control">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" name="submit" class="btn btn-primary">Lưu</button>
                </div>
            </div>
        </div>
    </form>

</body>

</html>