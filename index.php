<?php
include 'db_connect.php';
if(isset($_GET["status"]))
{
    if($_GET["status"]=="success")
    {
        echo "<script>alert('Thêm mới thành công!')</script>";
    }

    else if($_GET["status"]=="update_success")
    {
        echo "<script>alert('Cập nhật sửa đổi thành công!')</script>";
    }

    else if ($_GET["status"]=="delete_success")
    {
        echo "<script>alert('Xóa thành công!')</script>";
    }
}

?>
<!DOCTYPE html>
<html>

<head>
    <title>Quản lí học sinh</title>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js">
    </script>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-4">
                <div class="pull-left" style=" color: blue">
                    <h2>Danh sách học sinh</h2>
                </div>
            </div>
            <div class="col-xs-8">
                <div class="row">
                    <div class="col-md-4 col-md-push-8">
                        <div class="pull-right" style="margin-top: 20px">
                            <a class="btn btn-success" href="create.php"> Thêm mới </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-md-pull-4" style="margin-top:25px; margin-left: 10%">
                        <div class="box">
                            <div class="container-1">

                                <form method="post" action="index.php">
                                    <input type="search" name="search" placeholder="Search..." />
                                    <button type="submit" name="submit"><span class="icon"><i
                                                class="fa fa-search"></i></span></button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <?php
            if(isset($_POST["search"]))
            {
                 $search=$_POST["search"] ;
                 
            }
             else { $search="" ; }


             $sql = mysqli_query($conn,"select * from hocsinh where HoTen like '%$search%'");

             if(mysqli_num_rows($sql) > 0){
                 echo '<table id="example" class="display" style="width:100%">
                        <thead>
                            <tr>
                                 <th>Mã học sinh</th>
                                 <th>Họ tên</th>
                                 <th>Ngày sinh</th>
                                 <th>Lớp</th> 
                                 <th>Trạng thái</th> 
                             </tr>
                         </thread>';
				echo '<tbody>';
                 while($row = mysqli_fetch_assoc($sql)){
                    
                     echo '<tr>
                         <td>'.$row['MaHS'].'</td>
                         <td>'.$row['HoTen'].'</td>
                         <td>'.$row['NgaySinh'].'</td>
                         <td>'.$row['Lop'].'</td>
                         <td>
                         <a class="btn btn-warning" href="detail.php?id='.$row['MaHS'].'"> Detail </a> |
                         <a class="btn btn-primary" href="update.php?id='.$row['MaHS'].'"> Edit </a> |
                         <a class="btn btn-danger" onclick="return confirm(\'Bạn có chắc chắn muốn xóa học sinh này không?\')" href="deleted.php?id='.$row['MaHS'].'"> Delete </a>
                         </td>
                     </tr>';
         
                 }
				 echo '</tbody>';
				  echo '<tfoot>
                             <tr>
                                 <th>Mã học sinh</th>
                                 <th>Họ tên</th>
                                 <th>Ngày sinh</th>
                                 <th>Lớp</th> 
                                 
                             </tr>
                         </tfoot>';
                 echo '</table>';
             }
             else
             {
                 echo "<h3>No records found. Please insert some records</h3>";
             }
         
?>
    </div>
</body>
<script>
$(document).ready(function() {
    $('.delete').click(function() {
        $('#input_delete').val = $(this).data("id");
    })
})
$(document).ready(function() {
    $('#example').DataTable({
        initComplete: function() {
            this.api().columns().every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function(d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });
});
</script>

</html>